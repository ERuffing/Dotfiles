#!/bin/bash
#
# This script will install or update the dotfiles installation.

echo Updating git submodules.
git submodule update --init --recursive

# Vim links
echo Making links for vim profile:
if [ ! -h ~/.vim ]; then
	if [ -e ~/.vim ]; then
		echo A \'.vim\' directory is already present in your home directory. This
		echo directory will be moved to \'~/.vim.bak\'.
		mv -v ~/.vim ~/.vim.bak
	fi
	ln -s -v "$PWD/vim/_vim" ~/.vim
else
	echo A link is already present at \'~/.vim\'.
fi
if [ ! -h ~/.vimrc ]; then
	if [ -e ~/.vimrc ]; then
		echo A \'.vimrc\' file is already present in your home directory. This file
		echo will be moved to \'~/.vimrc.bak\'.
		mv -v ~/.vimrc ~/.vimrc.bak
	fi
	ln -s -v "$PWD/vim/_vimrc" ~/.vimrc
else
	echo A link is already present at \'~/.vimrc\'.
fi


# NeoVim links
echo Making links for neovim profile:
mkdir -p ~/.config/nvim
if [ ! -h ~/.config/nvim/init.vim ]; then
	if [ -e ~/.config/nvim/init.vim ]; then
		echo A \'.config/nvim/init.vim\' file is already present in your home directory. This file
		echo will be moved to \'~/.config/nvim/init.vim.bak\'.
		mv -v ~/.config/nvim/init.vim ~/.config/nvim/init.vim.bak
	fi
	ln -s -v "$PWD/nvim/init.vim" ~/.config/nvim/init.vim
else
	echo A link is already present at \'~/.config/nvim/init.vim\'.
fi


# Bash profile links
echo Making links for Bash profile:
if [ ! -h ~/.bashrc ]; then
	if [ -e ~/.bashrc ]; then
		echo A \'.bashrc\' file is already present in your home directory. This file
		echo will be moved to \'~/.bashrc.bak\'.
		mv -v ~/.bashrc ~/.bashrc.bak
	fi
	ln -s -v "$PWD/bash/bashrc" ~/.bashrc
else
	echo A link is already present at \'~/.bashrc\'.
fi
if [ ! -h ~/.bash_logout ]; then
	if [ -e ~/.bash_logout ]; then
		echo A \'.bash_logout\' file is already present in your home directory. This
		echo file will be moved to \'~/.bash_logout.bak\'.
		mv -v ~/.bash_logout ~/.bash_logout.bak
	fi
	ln -s -v "$PWD/bash/bash_logout" ~/.bash_logout
else
	echo A link is already present at \'~/.bash_logout\'.
fi
if [ ! -h ~/.bash_aliases ]; then
	if [ -e ~/.bash_aliases ]; then
		echo A \'.bash_aliases\' file is already present in your home directory.
		echo This file will be moved to \'~/.bash_aliases.bak\'.
		mv -v ~/.bash_aliases ~/.bash_aliases.bak
	fi
	ln -s -v "$PWD/bash/bash_aliases" ~/.bash_aliases
else
	echo A link is already present at \'~/.bash_aliases\'.
fi

# Zsh profile links
echo Making links for Zsh profile:
if [ ! -h ~/.oh-my-zsh ]; then
	if [ -e ~/.oh-my-zsh ]; then
		echo A \'.oh-my-zsh\' directory is already present in your home
		echo directory. This directory will be moved to \'~/.oh-my-zsh.bak\'.
		mv -v ~/.oh-my-zsh ~/.oh-my-zsh.bak
	fi
	ln -s -v "$PWD/zsh/oh-my-zsh" ~/.oh-my-zsh
else
	echo A link is already present at \'~/.oh-my-zsh\'.
fi
if [ ! -h ~/.zshrc ]; then
	if [ -e ~/.zshrc ]; then
		echo A \'.zshrc\' file is already present in your home directory. This
		echo file will be moved to \'~/.zshrc.bak\'.
		mv -v ~/.zshrc ~/.zshrc.bak
	fi
	ln -s -v "$PWD/zsh/zshrc" ~/.zshrc
else
	echo A link is already present at \'~/.zshrc\'.
fi
if [ ! -h ~/.aliases ]; then
	if [ -e ~/.aliases ]; then
		echo A \'.aliases\' file is already present in your home directory. This
		echo file will be moved to \'~/.aliases.bak\'.
		mv -v ~/.aliases ~/.aliases.bak
	fi
	ln -s -v "$PWD/zsh/aliases" ~/.aliases
else
	echo A link is already present at \'~/.aliases\'.
fi

read -p "Set /bin/zsh as default shell? [y/N]" -i N setZshDefault
if [[ $setZshDefault == "Y" || $setZshDefault == "y" ]]; then
	chsh -s /bin/zsh
fi


# Set up user directory structure
echo Setting up local directory structure:
if [ ! -e ~/.local ]; then
	echo \t- Creating \'~/.local\'
	mkdir ~/.local
fi
if [ ! -e ~/.local/bin ]; then
	echo \t- Creating \'~/.local/bin\'
	mkdir ~/.local/bin
fi

# Set up Linuxbrew
read -p "Install linuxbrew? [y/N] " installLinuxBrew
[ -z "${installLinuxBrew}" ] && installLinuxBrew='N'
if [[ $installLinuxBrew == "Y" || $installLinuxBrew == "y" ]]; then
	if ! [ -x "$(command -v brew)" ]; then
		echo Installing Linuxbrew:
		/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
		(echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> ~/.zprofile
	    eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
	else
		echo Brew command already found, not installing.
	fi
	read -p "Install common utilities via brew? [y/N] " installUtilities
	[ -z "$installUtilities" ] && installUtilities='N'
	if [[ $installUtilities == "Y" || $installUtilities == "y" ]]; then
		if ! [ -x "$(command -v sl)" ]; then
			brew install sl
		fi
		if ! [ -x "$(command -v nvim)" ]; then
			brew install neovim
		fi
		if ! [ -x "$(command -v htop)" ]; then
			brew install htop
		fi
		if ! [ -x "$(command -v hx)}"]; then
			brew install helix
		fi
	fi
fi

# Optionally install rust
read -p "Install rust? [y/N] " installRust
[ -z "${installRust}" ] && installRust='N'
case $installRust in
	y|Y) curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh ;;
	*) echo Skipping rust installation ;;
esac
