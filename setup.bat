MKLINK /D %userprofile%\.oh-my-zsh .\zsh\oh-my-zsh
MKLINK /D %userprofile%\.vim .\vim\_vim
MKLINK /D %userprofile%\vimfiles .\vim\_vim
MKLINK %userprofile%\.aliases .\zsh\aliases
MKLINK %userprofile%\.zshrc .\zsh\zshrc
MKLINK %userprofile%\.vimrc .\vim\_vimrc
MKLINK %userprofile%\_vimrc .\vim\_vimrc
