Things to Install
=================
Remember to install these packages when setting up a new machine:

1. On Ubuntu/Debian-based distributions
    * `linux-headers-generic`
    * `python-software-properties`
    * `synaptic`
2. On RHEL/CentOS/Fedora/etc. distributions
    * `kernel-devel`
2. `gcc`
5. `pandoc`
6. `vim`
7. `emacs`
8. `git`
9. git flow
    * On Ubuntu/Debian-based distributions, use `git-flow`
    * On RHEL/CentOS/Fedora/etc. distributions, use `gitflow`
10. `zsh`
    * Be sure to run the following after install zsh in order to set it as
       your default shell:
```sh
chsh -s /bin/zsh
```
11. TeX Live
    * Be sure to get the version from the
      [TeX Live website](https://www.tug.org/texlive/acquire-netinstall.html),
      not the version included with the OS distribution (the included
      version is broken).
12. [https://atom.io/](Atom)
13. [https://www.google.com/chrome/](Google Chrome)
14. Doxygen
