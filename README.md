Dotfiles
========
Ethan Ruffing

Overview
--------
This is a personal collection of preference files (commonly referred to as
"dotfiles") that I have created in order to make transitioning between
machines easy.

Installation
------------
To install, clone the git repository to a desired location. Then, just run the
appropriate setup script (`setup.sh` or `setup.bat`).
